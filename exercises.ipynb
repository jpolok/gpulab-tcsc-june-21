{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "resident-travel",
   "metadata": {},
   "source": [
    "---\n",
    "### Welcome to the GPU exercises of the topical CERN School of Computing Spring 2021 edition!\n",
    "\n",
    "Please follow the steps in this notebook. All underlying code can be found in the directories of the project. The files can be opened with an editor by clicking on the respective files. You can obtain syntax highlighting for .cu files by choosing `LANGUAGE` -> `C++` from the menu. We highly recommend that you inspect the source code of every exercise. They are linked as used in the examples below.\n",
    "\n",
    "You can execute the code in the code execution cells below by pressing `CTRL` + `ENTER`. The output will appear just below the cell. Sometimes it the execution can take a little while, so be a bit patient. Note that compilation commands do not produce output when running successfully. \n",
    "\n",
    "When CUDA programs are executed in the exercise setup, we always call a small script, called `run-exclusive` to ensure that no other user is using the GPU assigned to you at the same time. This is necessary since we have fewer GPUs available for the session than students attending. If the program is not launched for about 30 seconds, just try again. If it still does not work, you can inspect the processes running on your GPU with `nvidia-smi` (see instructions below). If in doubt, please get in touch with the organizers. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "collective-pickup",
   "metadata": {},
   "source": [
    "### Exploring the GPU status\n",
    "\n",
    "Let us first explore the GPU available for you in this lab environment. It is an accelerated system, containing a GPU assigned to you (and a fellow student). `nvidia-smi` (Systems Management Interface) is a utility shipped with CUDA that monitors the processes running on the GPU and provides some information. It is often useful to check whether another user is using the same GPU.\n",
    "\n",
    "*Sidenote: `nvidia-smi` also tells you the exact driver and CUDA version. This can be useful information if after a new installation / an update there is a mismatch between the CUDA and driver versions (nothing to be worried about for this lab).*\n",
    "\n",
    "Not down the GPU name, memory available and whether a process is currently running on the GPU! "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "imposed-bloom",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!nvidia-smi"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "thirty-duplicate",
   "metadata": {},
   "source": [
    "### Compile a CUDA program\n",
    "\n",
    "Let us now compile and run a small program, [`device_properties.cu`](edit/device_properties/device_properties.cu) (*<---- click on the link of the source file to open it in another tab for editing*), which will give more detailed information about the GPU(s) available. This program does not do any calculations on the GPU, it simply queries information about it and shows you which function calls are available for that. \n",
    "\n",
    "`.cu` is the extension for CUDA accelerated files. To compile a CUDA file, we use the `nvcc` compiler, which compiles both the host and the device sections of the code. Its usage is very similar to `gcc`. Let's take a closer look at the following command:\n",
    "- `nvcc` invokes the compiler from the command line\n",
    "- `-arch` indicates the GPU architecture for which the file is compiled. For our GPU this is `sm_70`. For more information on the architecture, please refer to the [CUDA documentation](https://docs.nvidia.com/cuda/cuda-compiler-driver-nvcc/index.html#options-for-steering-gpu-code-generation)\n",
    "- `-o` specifies the output file (i.e. the compiled program) \n",
    "- `device_properties/device_properties.cu` is the file to compile"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "hollow-stomach",
   "metadata": {},
   "outputs": [],
   "source": [
    "!nvcc -arch=sm_70 -o device_properties/device_properties device_properties/device_properties.cu "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "engaged-platform",
   "metadata": {},
   "source": [
    "### Exploring some GPU characteristics\n",
    "\n",
    "After successfully compiling your first CUDA program, we can now execute it. This is done by calling the output file produced above, i.e. `./device_properties/device_properties`. In our case we call it with the wrapper script [`run-exclusive.sh`](edit/run-exclusive.sh) described above which ensures that only a single process runs on the GPU at once. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "later-testament",
   "metadata": {},
   "outputs": [],
   "source": [
    "!./run-exclusive.sh ./device_properties/device_properties"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "unknown-complement",
   "metadata": {},
   "source": [
    "Note down the following: How large can a grid be maximally on this GPU? How much global and shared memory is available? What are the restrictions on the block size? How large is the warp size? How many Streaming Multiprocessors are there on the GPU? Can you infer from the information that the compute architecture of this GPU is indeed `sm_70`?\n",
    "\n",
    "*Side note for when you work on any CUDA server: A similar program is available in the CUDA samples which are installed with every CUDA installation. You can find them in the directory `cuda-samples` of your CUDA installation directory (typically `/usr/local/cuda`). The scripts in `1_Utilities` can be useful. In particular, `1_Utilities/deviceQuery` gives similar (and more) information than the `device_properties`program we provide here.*"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "pending-academy",
   "metadata": {},
   "source": [
    "### Hello World from the GPU\n",
    "\n",
    "We are now ready to write our first own function for GPUs, starting off from the following code: [`hello_world/hello_world.cu`](../edit/gpulab-tcsc-june-211/hello_world/hello_world.cu). This code comes with a CPU function and already compiles, but only calls the CPU version of the function. Your task is to modify the code to also provide a working GPU function and to invoke it.\n",
    "\n",
    "The idea is to print a message from every invocated kernel to the terminal. For this, modify the GPU function such that it is actually executed on the GPU and invoke the GPU function. Open the file in a separate tab in an editor, modify it according to the instructions given in the file (marked with *to do*) and remember to save your changes. Then you can compile and run it with the commands given below.\n",
    "\n",
    "Remember that a GPU kernel is launched with the following syntax: `somKernel<<<number_of_blocks, number_of_threads>>>()`. `someKernel` is then executed for every thread in every block, so `number_of_blocks` * `number_of_threads` times. `somKernel<<<1, 1>>>()` launches only one instance: one thread in one block. `somKernel<<<1, 32>>>()` launches 32 instances: 32 threads in one block. `somKernel<<<2, 32>>>()` launches 64 instances: two blocks with 32 threads each.\n",
    "\n",
    "Each thread is identified by an index, starting from 0, and each block is identified by an index starting from 0. \n",
    "To identify within the CUDA kernel code which instance of the kernel is processed, the pre-defined variables `blockIdx.x` and `threadIdx.x` are available to identify the index of the block and the index of the thread within the block. Note their usage in our `hello_world_gpu` function. \n",
    "\n",
    "If you are stuck or would like to have some inspiration, you can take a look at the [solution](../edit/gpulab-tcsc-june-211/hello_world/hello_world_solution.cu)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "wanted-princess",
   "metadata": {},
   "outputs": [],
   "source": [
    "!nvcc -arch=sm_70 -o hello_world/hello_world hello_world/hello_world.cu"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "smooth-advancement",
   "metadata": {},
   "outputs": [],
   "source": [
    "!./run-exclusive.sh ./hello_world/hello_world 1 1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "weighted-sussex",
   "metadata": {},
   "source": [
    "Congratulations! You just processed your first function on a GPU! \n",
    "\n",
    "Let's explore a bit deeper. The program takes the following input parameters: the number of threads per block and the number of blocks in the grid (both set to one in the above program call). Try experimenting with different settings! In particular, try at least 64 or 96 threads per block. What pattern do you observe in the printout. What could it be due to?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "detailed-address",
   "metadata": {},
   "outputs": [],
   "source": [
    "!./run-exclusive.sh ./hello_world/hello_world 3 64"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "adequate-modern",
   "metadata": {},
   "source": [
    "### Vector addition\n",
    "We are now ready to move on to an exercise where data is copied to and from the GPU and calculations are executed on the GPU: a vector addition. Start from the code provided in [vector_addition.cu](../edit/gpulab-tcsc-june-211/vector_addition/vector_addition.cu) and follow the instructions below and in the code. \n",
    "The provided code allocates memory on the host and runs the vector addition on the GPU. \n",
    "Our goal now is to allocate the required memory on the GPU, copy the input from the host to the GPU, call the vector addition in parallel on the GPU and copy the result back to the host to check that it is correct.\n",
    "\n",
    "The initial version of the code compiles and runs. It takes three input parameters: the size of the vectors to be added, the number of blocks in the grid and the number of threads per block. Note that the last two parameters will only be relevant once we parallelize the vector addition on the GPU in Step 2.\n",
    "\n",
    "Check after every of the below steps that your code compiles and runs!\n",
    "\n",
    "As before, if you are stuck or need inspiration you can take a look at the [solutons](../edit/gpulab-tcsc-june-211/vector_addition/vector_addition_solution.cu)\n",
    "\n",
    "#### Step 1: Allocating memory\n",
    "To do a vector addition on the GPU, we have to allocate GPU memory (global memory) for the input vectors (called `a` and `b`) and also for the vector, where the result is stored (called `c`). The input vectors have to be copied from host memory to GPU global memory and in the end the result vector has to be copied from the GPU to the host. \n",
    "\n",
    "Note that in the code we label host variables with `_h` in the end and device variables with `_d` in the end. This is common practice in CUDA programs to distinguish between pointers to host and device memory. \n",
    "\n",
    "Follow the instructions in the code labelled with *Step 1 to do*. There are three places labelled like that to\n",
    "- Allocate GPU global memory for the three device vectors `a_d`, `b_d` and `c_d`\n",
    "- Copy the data in the host vectors `a_h`, `b_h`, `c_h` to the device vectors `a_d`, `b_d`, `c_d`\n",
    "- Free the global memory used for the device vectors `a_d`, `b_d`, `c_d`\n",
    "\n",
    "Test that your code compiles and runs!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "positive-lawsuit",
   "metadata": {},
   "outputs": [],
   "source": [
    "!nvcc -arch=sm_70 -o vector_addition/vector_addition vector_addition/vector_addition.cu"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "electoral-active",
   "metadata": {},
   "outputs": [],
   "source": [
    "!./run-exclusive.sh ./vector_addition/vector_addition 36 6 6"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "severe-acrobat",
   "metadata": {},
   "source": [
    "#### Step 2: Vector addition in parallel on the GPU\n",
    "\n",
    "It is now time to call `vector_addition_gpu` on the GPU and to ensure that the addition is carried out in parallel. For this, follow the instructions labelled with *Step 2 to do* to do the following:\n",
    "- Label `vector_addition_gpu` with the `__global__` identifier\n",
    "- Modify the for loop inside `vector_addition_gpu` to be executed in parallel (see explanations below)\n",
    "- Uncomment the grid dimension variable definitions\n",
    "- Launch the kernel \n",
    "\n",
    "For loops are ideal candidates to be processed in parallel if the iterations do not depend on one another, as is the case in vector addition. The idea is instead of running each iteration of the loop sequentially, the iterations are processed in parallel by all available threads. For this two things must happen: 1) The kernel is written to execute one iteration based on its thread and block index and 2) we must ensure that all iterations of the for loop are processed, irrespectively of how many threads and blocks the kernel was launched with. Note that for this to work you should use the known `threadIdx.x`, `blockIdx.x`, `blockDim.x` and `gridDim.x` variables. \n",
    "\n",
    "Now test again that your code compiles and runs!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "binary-leather",
   "metadata": {},
   "outputs": [],
   "source": [
    "!nvcc -arch=sm_70 -o vector_addition/vector_addition vector_addition/vector_addition.cu"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "remarkable-brown",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "sufficient-giant",
   "metadata": {},
   "outputs": [],
   "source": [
    "!./run-exclusive.sh ./vector_addition/vector_addition 36 6 6"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "naval-midwest",
   "metadata": {},
   "source": [
    "#### Step 3: Copy and verify result\n",
    "\n",
    "As last step, we have to copy back the vector containing the result and verify that the computations executed on the GPU were correct. Follow the instructions labelled with *Step 3 to do* for this. \n",
    "\n",
    "- Copy content of `c_d` to `c_h`\n",
    "- Synchronize to ensure that the GPU work is finished\n",
    "- Verify the result obtained from the GPU\n",
    "\n",
    "Now compile and run again to check that your first calculations on a GPU are working!\n",
    "\n",
    "Play with the number of blocks and threads and test scenarios where the `n_threads` * `n_block` is not equal to the vector size. If this works, you have correctly parallelized your for loop. \n",
    "\n",
    "Caveat: The restult vector can be correct, but the parallelization might not be the intended one. This can happen if you are in fact doing the same work in every block of your grid. If you did not use the variables `blockDim.x` and `gridDim.x` this happened. Take a look again at your parallelized for loop and modify it such that every block in the grid processed different vector elements from the other blocks. Check again that you can process vector lengths that do not match the number of blocks and threads!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "consistent-marks",
   "metadata": {},
   "outputs": [],
   "source": [
    "!nvcc -arch=sm_70 -o vector_addition/vector_addition vector_addition/vector_addition.cu"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "advised-buffer",
   "metadata": {},
   "outputs": [],
   "source": [
    "!./run-exclusive.sh ./vector_addition/vector_addition 39 6 6"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Raw Cell Format",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
