Heterogeneous Computing - tCSC June 2021
============

Welcome to the Heterogeneous Computing lab session of tCSC June 2021.

Open the jupyter notebook containing the exercises `exercises.ipynb` by clicking on it and follow the instructions there.

Enjoy!
